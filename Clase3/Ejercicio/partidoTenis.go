package main

import (
	"fmt"
	"time"
)

type Player struct {
	name       string
	score      int
	matchPoint bool
}

type Game struct {
	player1 Player
	player2 Player
}

func (player Player) String() string {
	puntajes := [4]string{"0", "15", "30", "40"}
	return fmt.Sprint(player.name + " " + puntajes[player.score]) //+ " " + strconv.FormatBool(*player.matchPoint)
}

const POINT_WIN int = 4
const POINT_MATCH_POINT int = 3

func main() {

	continues := true

	game := Game{
		Player{name: "DelPotro"}, //, score: new(int), matchPoint: new(bool)},
		Player{name: "Djokovic"}} //, score: new(int), matchPoint: new(bool)}}

	for continues {
		continues, game = round(game)
	}

	if game.player1.score == POINT_WIN {
		fmt.Println("Win is", game.player1.name, "!!")
	} else {
		fmt.Println("Win is", game.player2.name, "!!")
	}

}

func round(game Game) (bool, Game) {

	fmt.Println("\nReally go!!")

	game = startPlay(game)

	game = isMatchPoint(game)

	game = isDeuse(game)

	if game.player1.score == POINT_WIN || game.player2.score == POINT_WIN {
		return false, game
	} else {
		fmt.Printf("Scoring: %v - %v \n", game.player1, game.player2)
		return true, game
	}
}

func startPlay(game Game) Game {
	player1HitChan := make(chan string)
	player2HitChan := make(chan string)

	go func() { time.Sleep(time.Second * 1); player1HitChan <- "Gol!!" }()

	go func() { time.Sleep(time.Second * 1); player2HitChan <- "Gol!!" }()

	select {
	case <-player1HitChan:
		game.player1.score++
		if game.player1.score == POINT_MATCH_POINT {
			game.player1.matchPoint = true
		}
		fmt.Println("Hiit!! Point for", game.player1.name)
	case <-player2HitChan:
		game.player2.score++
		if game.player2.score == POINT_MATCH_POINT {
			game.player2.matchPoint = true
		}
		fmt.Println("Hiit!! Point for", game.player2.name)
	}

	return game
}

func isMatchPoint(game Game) Game {
	if game.player1.matchPoint == true && game.player1.score == POINT_MATCH_POINT && game.player2.matchPoint == false {
		fmt.Println("Match Point!!", game.player1.name)
	}
	if game.player2.matchPoint == true && game.player2.score == POINT_MATCH_POINT && game.player1.matchPoint == false {
		fmt.Println("Match Point!!", game.player2.name)
	}

	return game
}

func isDeuse(game Game) Game {
	if game.player1.matchPoint == true && game.player2.matchPoint == true {
		game.player1.matchPoint = false
		game.player2.matchPoint = false
		(game.player1.score)--
		(game.player2.score)--
		fmt.Println("Deuseee!!")
	}
	return game
}
