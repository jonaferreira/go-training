package main

import (
	"github.com/gin-gonic/gin"
)

func main() {
	router := gin.Default()

	router.GET("/calculate", func(c *gin.Context) {
		first := c.Query("first")
		second := c.Query("second")

		sum, err := calculator(first, second)

		c.JSON(200, gin.H{"first": first, "second": second})
	})

	router.Run()
}
