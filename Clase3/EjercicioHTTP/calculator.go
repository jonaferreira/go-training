package main

import (
	"fmt"
	"os"
	"strconv"
)

type CustomError struct {
	Message string
}

func (e *CustomError) Error() string {
	return e.Message
}

func asdad() {
	var cantValues = len(os.Args)
	fmt.Printf("Cantidad de parametros: %v\n", cantValues)

	if cantValues != 3 {
		fmt.Println("Cantidad de parametros mal")
		os.Exit(1)
	}
}

func calculator(firstParam string, secondParam string) (string, error) {

	var err error = &CustomError{Message: "It is a custom error"}

	first, err := validateParameter(firstParam)

	if err != nil {
		fmt.Println("First input parameter must be integer")
		os.Exit(1)
	}

	second, err := validateParameter(secondParam)

	if err != nil {
		fmt.Println("Second input parameter must be integer")
		os.Exit(1)
	}

	if 0 < first && first < second && (first%3 == 0 || first%5 == 0) {
		fmt.Printf("La suma de los parametros es: %v\n", first+second)
	} else {
		fmt.Println("Los parametros no son permitidos")
	}

}

func validateParameter(parameter string) (int64, error) {
	value, err := strconv.ParseInt(parameter, 10, 0)
	return value, err
}
