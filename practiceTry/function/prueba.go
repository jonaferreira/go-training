package main

import "fmt"

type fn func(string)

func main() {
	f := getFunction()
	f("test numero uno")
	//executeFunction(getFunction(), "test")
	executeFunction(f, "")

	// Lambda
	func(n string) {
		fmt.Println("Hello Lambda", n)
	}("Gophers")

	// Closure
	//value := "Gophers"
	func() {
		fmt.Println("Hello Closure", "Esto es una prueba") //value)
	}()
}

func getFunction() func(string) {
	f := func(value string) {
		fmt.Printf("Execute function with value: %s \n", value)
	}
	return f
}

func executeFunction(f fn, value string) {
	f(value)
}
