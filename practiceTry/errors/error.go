package main

import (
	"errors"
	"fmt"
)

// errorString is a trivial implementation of error.
type errorString struct {
	s string
}

func (e *errorString) Error() string {
	return e.s
}

// New returns an error that formats as the given text.
func New(text string) error {
	return &errorString{text}
}

func Sqrt(f float64) (float64, error) {
	if f < 0 {
		return 0, errors.New("math: square root of negative number")
	}
	return 1, nil
}

func main() {
	var f float64 = 0.0

	f, err := Sqrt(-1)

	if err != nil {
		fmt.Println(err)
	}

	fmt.Println(f)
}
