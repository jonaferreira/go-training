package main

import (
	"fmt"
	"math"
	"os"
	"strconv"
)

func main() {
	var pesos float32 = 1.2
	fmt.Printf("Pesos: %v ", pesos)

	args := []string{"arg1", "arg2", "arg3", "arg4"}

	fmt.Println("Valor: " + args[1])

	var x, y int = 3, 4
	var f float64 = math.Sqrt(float64(x*x + y*y))
	var z uint = uint(f)
	fmt.Println(x, y, z)

	var cantValues = len(os.Args)
	fmt.Printf("Cantidad de parametros: %v\n", cantValues)

	if cantValues != 3 {
		fmt.Println("Cantidad de parametros mal")
		os.Exit(1)
	}

	// convert input (type string) to integer
	first, err := strconv.ParseInt(os.Args[1], 10, 0)

	if err != nil {
		fmt.Println("First input parameter must be integer")
		os.Exit(1)
	}

	second, err := strconv.ParseInt(os.Args[2], 10, 0)

	if err != nil {
		fmt.Println("Second input parameter must be integer")
		os.Exit(1)
	}

	fmt.Printf("Primer valor: %v \t Segundo valor: %v\n", first, second)

	if 0 < first && first < second && (first%3 == 0 || first%5 == 0) {
		fmt.Printf("Parametros son permitidos: 0 < %v < %v \n", first, second)
		fmt.Printf("La suma de los parametros es: %v\n", first+second)
	} else {
		fmt.Println("Los parametros no son permitidos")
	}
}
