package main

import (
	"fmt"
	"os"
	"strconv"
)

func validateParameter(parameter string) int64 {
	value, err := strconv.ParseInt(parameter, 10, 0)
	if err != nil {
		fmt.Printf("ERROR! - %v - The input parameter must be integer", parameter)
		os.Exit(1)
	}
	return value
}

func validateCantParameter(parameters []string) {
	if len(parameters) <= 1 {
		fmt.Println("Debe ingresar parametros")
		os.Exit(1)
	}
}
