package main

import (
	"os"
)

func main() {
	var slice = createSlice(os.Args)
	var tree = createTreeFromSlice(slice)
	printTreeInOrden(tree)
}

func printTreeInOrden(t *tree) {
	inOrden(t)
}

func createTreeFromSlice(slice []int64) *tree {
	var tree *tree
	for _, value := range slice {
		tree = add(tree, value)
	}
	return tree
}

func createSlice(arguments []string) []int64 {
	validateCantParameter(os.Args)

	var s2 []int64

	for i := 1; i < len(os.Args); i++ {
		var value = validateParameter(os.Args[i])
		s2 = append(s2, value)
	}

	return s2
}
