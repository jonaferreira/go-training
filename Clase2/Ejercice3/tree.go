package main

import "fmt"

type tree struct {
	value       int64
	left, right *tree
}

func add(t *tree, value int64) *tree {
	if t == nil {
		// Equivalent to return &tree{value: value}.
		t = new(tree)
		t.value = value
		return t
	}
	if value < t.value {
		t.left = add(t.left, value)
	} else {
		t.right = add(t.right, value)
	}
	return t
}

func inOrden(t *tree) {
	if t != nil {
		inOrden(t.left)
		fmt.Println(t.value)
		inOrden(t.right)
	}
}
