package main

import (
	"fmt"
	"os"
	"strconv"
)

func main() {

	var cantValues = len(os.Args)
	fmt.Printf("Cantidad de parametros: %v\n", cantValues)

	if cantValues != 3 {
		fmt.Println("Cantidad de parametros mal")
		os.Exit(1)
	}

	first, err := validateParameter(os.Args[1])

	if err != nil {
		fmt.Println("First input parameter must be integer")
		os.Exit(1)
	}

	second, err := validateParameter(os.Args[2])

	if err != nil {
		fmt.Println("Second input parameter must be integer")
		os.Exit(1)
	}

	sumParametersIfSecondIsMultOfThree(first, second)

}

func validateParameter(parameter string) (int64, error) {
	value, err := strconv.ParseInt(parameter, 10, 0)
	return value, err
}

func sumParametersIfSecondIsMultOfThree(first int64, second int64) {
	if 0 < first && first < second && (first%3 == 0 || first%5 == 0) {
		fmt.Printf("La suma de los parametros es: %v\n", first+second)
	} else {
		fmt.Println("Los parametros no son permitidos")
	}
}
