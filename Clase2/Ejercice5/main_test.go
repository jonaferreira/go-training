package main

import "testing"

func TestValidateParameterWithValidParameter(t *testing.T) {
	parameter, err := validateParameter("1")

	if err != nil {
		t.Error("The input parameter must be integer", parameter)
	}
}

func TestSumParametersIfSecondIsMultOfThree(t *testing.T) {
	parameter, err := validateParameter("a")

	if err != nil {
		t.Error("The input parameter must be integer", parameter)
	}
}
