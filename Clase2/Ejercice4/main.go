package main

import (
	"fmt"
	"os"
)

type General struct {
    Price float64
	Discount float64
}

func (gral General) netoPrice() float64 {
	fmt.Printf(" - Discount:\t%v%%\n",gral.Discount)
    return gral.Price
}

type Retiree struct {
	Price float64
	Discount float64
}

func (ret Retiree) netoPrice() float64 {
	var discount = ret.Discount / 100.0
	fmt.Printf(" - Discount:\t%v%%\n", ret.Discount)
	return ret.Price * discount
}

type Guest struct {
	Price float64
	Discount float64
}

func (guest Guest) netoPrice() float64 {
	fmt.Printf(" - Discount:\t%v%%\n",guest.Discount)
	return guest.Price * 0
}

type Asistence interface {
	netoPrice() float64
}

const brutePrice float64 = 500

func main() {
	validateCantParameter(os.Args, 1)

	var asist = validateParameter(os.Args[1])
	
	switch asist {
	case 1:
		fmt.Println("*** Ticket General ****")
		calculateNetoPrice(General{Price: brutePrice})
	case 2:
		fmt.Println("*** Ticket Retiree ****")
		calculateNetoPrice(Retiree{Price: brutePrice, Discount: 50 })
	case 3:
		fmt.Println("*** Ticket Guest ****")
		calculateNetoPrice(Guest{Price: brutePrice, Discount: 100 })
	default:
		fmt.Println("** Incorrect code ticket asistence **")
		fmt.Println("    The options are:")
		fmt.Println("     1 -- General")
		fmt.Println("     2 -- Retiree")
		fmt.Println("     3 -- Guest")
	}

}

func calculateNetoPrice(asist Asistence){
	fmt.Printf(" - Bruto Price:\t%v\n\n",brutePrice)
	fmt.Printf(" - Neto Price:\t%v\n", asist.netoPrice() )
}