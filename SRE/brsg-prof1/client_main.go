package main

import (
	"fmt"
	"net/http"
	"sync/atomic"
	"time"
)

const (
	requestLimitNewLine = 60
	useCircuitBreaker   = true
	targetAddress       = "localhost"
	targetPort          = 4002
	targetPath          = "work"
)

var (
	ticker       *time.Ticker
	targetURL    string
	requestCount uint32
)

func init() {
	targetURL = fmt.Sprintf("http://%s:%d/%s", targetAddress, targetPort, targetPath)
	requestCount = 0
}

func main() {
	fmt.Println("Cliente iniciado y solicitando trabajos al Servidor")

	var err error
	var resp *http.Response

	// inicializo un ticker para poder invocar ritmicamente al endpoint de healthCheck del server
	for ticker = time.NewTicker(100 * time.Millisecond); true; <-ticker.C {
		resp, err = http.Get(targetURL)
		if err != nil {
			fmt.Print("X")
		} else if resp.StatusCode >= http.StatusInternalServerError {
			fmt.Print("5")
		} else if resp.StatusCode >= http.StatusBadRequest {
			fmt.Print("4")
		} else {
			fmt.Print(".")
		}
		if resp != nil && resp.Body != nil {
			resp.Body.Close()
		}

		incrementRequestCount()
	}
}

func incrementRequestCount() {
	atomic.AddUint32(&requestCount, 1)
	if atomic.CompareAndSwapUint32(&requestCount, requestLimitNewLine, 0) {
		fmt.Println()
	}
}
