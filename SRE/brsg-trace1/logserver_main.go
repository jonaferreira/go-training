package main

import (
	"fmt"
	"net/http"

	// obtener las dependencias con: "go get -u DEPENDENCIA"
	"github.com/gin-gonic/gin"
)

const (
	serverPort = 4444
)

var (
	loggerChannelBuffer uint
	loggerChannel       chan string
)

func init() {
	loggerChannelBuffer = 100
	loggerChannel = make(chan string, loggerChannelBuffer)
}

func main() {
	router := gin.New()

	// declaramos los endpoints con sus handlers (Notaron la diferencia con http?)
	router.GET("/ping", pingHandler)
	router.POST("/log", logHandler)

	fmt.Println("Levantamos el background printer")
	go backgroundPrinter()

	// Levantamos un server con el middleware
	fmt.Println(fmt.Sprintf("Server iniciado y recibiendo logs en el puerto:%d", serverPort))
	router.Run(fmt.Sprintf(":%d", serverPort))
}

func pingHandler(ginCtx *gin.Context) {
	ginCtx.String(http.StatusOK, fmt.Sprintln("pong"))
}

func logHandler(ginCtx *gin.Context) {
	// empiezo mi trabajo
	var request map[string]string

	if err := ginCtx.ShouldBindJSON(&request); err != nil {
		ginCtx.JSON(http.StatusBadRequest, gin.H{"message": "invalid request"})
	}
	loggerChannel <- request["message"]
	// ...y respondo
	ginCtx.JSON(http.StatusAccepted, gin.H{"message": "accepted"})
}

func backgroundPrinter() {
	for mensaje := range loggerChannel {
		fmt.Println(mensaje)
	}
}
