package main

import (
	"bytes"
	"encoding/json"
	"flag"
	"fmt"
	"net/http"
	"sync/atomic"
	"time"

	// obtener las dependencias con: "go get -u DEPENDENCIA"
	uuid "github.com/satori/go.uuid"
)

const (
	requestLimitNewLine = 60
	useCircuitBreaker   = true
	defaultAPIName      = "Cliente"
	targetAddress       = "localhost"
	defaultTargetPort   = 4003
	targetPath          = "work"
)

var (
	ticker       *time.Ticker
	requestCount uint32
	apiName      string
	targetPort   uint
	targetURL    string
	loggerURL    string
	httpClient   http.Client
)

func init() {
	flag.StringVar(&apiName, "apiName", defaultAPIName, "nombre de este cliente")
	flag.UintVar(&targetPort, "targetPort", defaultTargetPort, "puerto del servidor de destino")
	flag.Parse()

	httpClient = http.Client{Timeout: time.Duration(1 * time.Second)}
	targetURL = fmt.Sprintf("http://%s:%d/%s", targetAddress, targetPort, targetPath)
	requestCount = 0

	loggerURL = "http://localhost:4444/log"
}

func main() {
	fmt.Println(fmt.Sprintf("Cliente iniciado y solicitando números al Servidor (%s)", targetURL))

	var req *http.Request
	var requestID string
	var err error
	var resp *http.Response

	// inicializo un ticker para poder invocar ritmicamente al endpoint de healthCheck del server
	for ticker = time.NewTicker(1 * time.Second); true; <-ticker.C {
		req, _ = http.NewRequest(http.MethodGet, targetURL, nil)
		if req != nil {
			UniqueID, _ := uuid.NewV4()
			requestID = UniqueID.String()

			req.Header.Set("X-Request-Id", requestID)

			startTime := time.Now()
			resp, err = httpClient.Do(req)
			endTime := time.Now()
			statusCode := -1
			if resp != nil {
				statusCode = resp.StatusCode
			}

			go backgroundRemoteLoggerPusher(startTime, endTime, requestID, statusCode)

			if err != nil {
				fmt.Print("X")
			} else if resp.StatusCode >= http.StatusInternalServerError {
				fmt.Print("5")
			} else if resp.StatusCode >= http.StatusBadRequest {
				fmt.Print("4")
			} else {
				fmt.Print(".")
			}
			if resp != nil && resp.Body != nil {
				resp.Body.Close()
			}
		} else {
			fmt.Print("x")
		}
		incrementRequestCount()
	}
}

func incrementRequestCount() {
	atomic.AddUint32(&requestCount, 1)
	if atomic.CompareAndSwapUint32(&requestCount, requestLimitNewLine, 0) {
		fmt.Println()
	}
}

func backgroundRemoteLoggerPusher(startTime, endTime time.Time, requestID string, statusCode int) {
	timeDelta := endTime.Sub(startTime)
	timeStamp := endTime.Format(time.RFC3339)
	logMessage := fmt.Sprintf("%s | api:%s | reqID:%s | status:%d | timeDelta:%f",
		timeStamp, apiName, requestID, statusCode, timeDelta.Seconds())

	requestBodyMap := make(map[string]string)
	requestBodyMap["message"] = logMessage
	if requestBodyBytes, marshalErr := json.Marshal(requestBodyMap); marshalErr == nil {
		http.Post(loggerURL, "application/json", bytes.NewBuffer(requestBodyBytes))
	}
}
