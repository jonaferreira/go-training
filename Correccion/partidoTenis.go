package main

import (
	"fmt"
	"time"
)

// Porque usaste punteros para los tipos de datos de score y matchPoint? Si no es necesario usar punteros,
// usa valores así evitás tener que dereferenciar
// que hace el código un poco menos legible.
type Player struct {
	name string
	// No es standard usar un tamaño de int específico si no hay un motivo para hacerlo.
	score      *int16
	matchPoint *bool
}

type Game struct {
	player1 Player
	player2 Player
}

// En Go podés definir el método String() que va a hacer que cuando se lo formatee en un printf se llame automaticamente para convertir tu struct a string
// Más info: https://stackoverflow.com/questions/13247644/tostring-function-in-go
func (player Player) toString() string {
	// Los puntos suspensivos hacen que declares un array con tamaño 4 (en go hay una diferencia importante entre arrays y slices,
	// generalmente se usan slices en vez de arrays porque pueden cambiar de tamaño mientras que los arrays tienen tamaño fijo,
	// para más info https://www.godesignpatterns.com/2014/05/arrays-vs-slices.html).
	// Ya que este array se redeclara cada vez que se llama la función quizás es buena idea declararlo fuera de la función y guardarlo en una variable.
	puntajes := [...]string{"0", "15", "30", "40"}
	return player.name + " " + puntajes[*player.score] //+ " " + strconv.FormatBool(*player.matchPoint)
}

//En go la convención para las variables constantes es camel case en vez de underscores.
const POINT_WIN int16 = 4
const POINT_MATCH_POINT int16 = 3

func main() {

	// Generalmente se declara una variable usando var cuando se quiere que tenga su zero value, si vas a asignarle un valor distinto setealo directamente
	// sin poner var o el tipo de dato "endGame := true"
	var endGame bool = true

	game := Game{
		Player{name: "DelPotro", score: new(int16), matchPoint: new(bool)},
		Player{name: "Djokovic", score: new(int16), matchPoint: new(bool)}}

	// Creo que las condiciones del loop quedarían más legibles haciendo algo así:
	// for continues {
	//	 continues = round(game)
	// }
	for endGame != false {
		endGame = round(game)
	}

	if *game.player1.score == POINT_WIN {
		fmt.Println("Win is", game.player1.name, "!!")
	} else {
		fmt.Println("Win is", game.player2.name, "!!")
	}

}

func round(game Game) bool {
	fmt.Println("")
	fmt.Println("Really go!!")

	startPlay(game)

	isMatchPoint(game)

	isDeuse(game)

	if *game.player1.score == POINT_WIN || *game.player2.score == POINT_WIN {
		return false
	} else {
		fmt.Printf("Scoring: %v - %v \n", game.player1.toString(), game.player2.toString())
		return true
	}
}

func startPlay(game Game) {
	player1HitChan := make(chan string)
	player2HitChan := make(chan string)

	// Es curioso esto. Usaste basicamente las goroutinas como un random. Como no sabemos cual de las dos se va a ejecutar primero termina siendo similar
	// a llamar rand.Float64() que devuelve un valor en [0.00, 1.00] y haciendo que si es menor a <= 0.5 gane Djokovic y sino gane el punto Del Potro.
	go func() { time.Sleep(time.Second * 1); player1HitChan <- "Gol!!" }()
	go func() { time.Sleep(time.Second * 1); player2HitChan <- "Gol!!" }()

	select {
	case <-player1HitChan:
		*game.player1.score++
		if *game.player1.score == POINT_MATCH_POINT {
			*game.player1.matchPoint = true
		}
		fmt.Println("Hiit!! Point for", game.player1.name)
	case <-player2HitChan:
		*game.player2.score++
		if *game.player2.score == POINT_MATCH_POINT {
			*game.player2.matchPoint = true
		}
		fmt.Println("Hiit!! Point for", game.player2.name)
	}
}

func isMatchPoint(game Game) {
	if *game.player1.matchPoint == true && *game.player1.score == POINT_MATCH_POINT && *game.player2.matchPoint == false {
		fmt.Println("Match Point!!", game.player1.name)
	}
	if *game.player2.matchPoint == true && *game.player2.score == POINT_MATCH_POINT && *game.player1.matchPoint == false {
		fmt.Println("Match Point!!", game.player2.name)
	}
}

func isDeuse(game Game) {
	if *game.player1.matchPoint == true && *game.player2.matchPoint == true {
		*game.player1.matchPoint = false
		*game.player2.matchPoint = false
		// Me quede con las ganas de ver ventaja para Del Potro en vez de que los scores vuelvan a 30 - 30 cuando están en 40 - 40 aunque en la práctica sea lo mismo.
		*(game.player1.score)--
		*(game.player2.score)--
		fmt.Println("Deuseee!!")
	}
}
